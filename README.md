#VIPLE_Gaming_Unity#

##Overview##

Three Unity Simulators for ASU VIPLE in the form of minigames

1. Snake Game

2. Space Sidescroller Game

3. Runner Game

##Instructions##

###1) Install VIPLE

http://neptune.fulton.ad.asu.edu/VIPLE/

###2) Install Unity 

https://unity.com/

###3) Run the Unity project in src, or start the built release executable in bin.

Open one of the following scenes. You can also press 1, 2, and 3 to cycle between them.

* Scenes/SnakeGame.Unity

* Scenes/SpaceInvaders.Unity

* Scenes/RunnerGame.Unity

###4) Start the VIPLE script for the specific minigame.

| Unity scene         	| VIPLE script          	|
|---------------------	|-----------------------	|
| SnakeGame.unity     	| UnityDriveSnake.viple 	|
| SpaceInvaders.unity 	| UnitySpaceGame2.viple 	|
| RunnerGame.unity    	| UnityRunner.viple     	|

Make sure to run the VIPLE script after the Unity app is already running, or the Unity editor is already in play mode.

###5) Play the game from VIPLE or within Unity using the instructions on the screen, or the comments in VIPLE

####Snake Game

Unity: Left and right arrow keys to turn, R to reset

VIPLE: A and D to turn left and right

####Space Game

Unity: Left and right arrow keys to shift left and right, click to fire laser

VIPLE: A and D to shift left and right, W to fire laser

####Runner Game

Unity: Space to jump

VIPLE: D to jump

##Screenshots##

![Snake game](Screenshots/snakeview.PNG)

![VIPLESnake](Screenshots/UnityDriveSnake.PNG)

![Space_game](Screenshots/Spacegameview.PNG)

##Links##

old repository:

https://bitbucket.org/c96/asu_viple_gaming