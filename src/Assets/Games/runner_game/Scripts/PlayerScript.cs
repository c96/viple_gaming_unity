﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

    public float jumpStrength = 200.0f;
    Rigidbody2D rb;
    public bool isOnGround;

    public bool jumpFlag;

    // Use this for initialization
    void Start() {
        jumpFlag = false;
        rb = transform.GetComponent<Rigidbody2D>();
    }

    public void SetJump() {
        jumpFlag = true;
    }

    public void RobotRightTurn()
    {
        Debug.Log("Right turn not allowed");
        // no turning
    }

    public void RobotLeftTurn()
    {
        Debug.Log("Left turn not allowed");
        // no turning
    }

    // forward represents jump for the runner
    public void RobotForward()
    {
        Debug.Log("Received forward message.");
        SetJump();
    }


    void FixedUpdate () {
		if (Input.GetKeyDown(KeyCode.Space) && isOnGround)
        {
            rb.AddForce(Vector3.up * (jumpStrength * rb.mass * rb.gravityScale * 20.0f));
        }
        if (jumpFlag && isOnGround)
        {
            rb.AddForce(Vector3.up * (jumpStrength * rb.mass * rb.gravityScale * 20.0f));
            Debug.Log("Jump command from VIPLE");
            jumpFlag = false;
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.tag == "Ground")
        {
            isOnGround = true;
        }
    }

    void OnCollisionStay2D(Collision2D other)
    {
        if (other.collider.tag == "Ground")
        {
            isOnGround = true;
        }
    }

    void OnCollisionExit2D(Collision2D other)
    {
        if (other.collider.tag == "Ground")
        {
            isOnGround = false;
        }
    }
}
