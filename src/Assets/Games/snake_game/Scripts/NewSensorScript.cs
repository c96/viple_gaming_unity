﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewSensorScript : MonoBehaviour {

    RaycastHit hit;

    Vector3 directionVector;

    public VIPLENetworkController netController;

    public float distance;

    public void Start()
    {
        distance = -1.0f;
        // initial cube begins facing right
        //directionVector = transform.TransformDirection(Vector3.right);
        netController = GameObject.FindGameObjectWithTag("NetworkController").GetComponent<VIPLENetworkController>();
    }

    public void Update()
    {
        updateRotation();
        /*
         * if (Input.GetKeyDown(KeyCode.H))
        {
            transform.position += Vector3.back;
        }
        */
    }

    public void updateRotation()
    {
        directionVector = transform.TransformDirection(Vector3.right);
    }

    public void checkDistance()
    {
        if (Physics.Raycast(transform.position + directionVector, directionVector, out hit, 20.0f))
        {
            distance = hit.distance;
            if (tag == "RightSensor")
            {
                netController.setSensor(0, distance);
                Debug.Log(tag + " distance: " + distance);
            }
            else if (tag == "UpSensor")
            {
                netController.setSensor(1, distance);
                Debug.Log(tag + " distance: " + distance);
            }
            else if (tag == "LeftSensor")
            {
                netController.setSensor(2, distance);
                Debug.Log(tag + " distance: " + distance);
            }
            else if (tag == "DownSensor")
            {
                netController.setSensor(3, distance);
                Debug.Log(tag + " distance: " + distance);
            }
        }

        
    }
}
