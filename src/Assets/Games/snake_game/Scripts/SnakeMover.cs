﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SnakeMover : MonoBehaviour {

    bool hasEaten = false;

    bool alive = true;

    public GameObject tailPrefab;

    public GameObject sensorsMount;

    public FoodSpawn foodSpawn;

    public List<NewSensorScript> sensors;

    Vector3 dir = Vector3.right;

    List<Transform> tails = new List<Transform>();

    float timeFactor = 0.1f;
    float repeatFactor = 0.2f;

    bool rotateLeftNeeded;

    bool rotateRightNeeded;

    public void RobotRightTurn()
    {
        Debug.Log("Received right turn message.");
        receiveRotateRight();
    }

    public void RobotLeftTurn()
    {
        Debug.Log("Received left turn message.");
        receiveRotateLeft();
    }

    public void RobotForward()
    {
        Debug.Log("Forward not allowed.");
        // no forward command for snake
    }


    void Start()
    {
        rotateLeftNeeded = false;
        rotateRightNeeded = false;
        InvokeRepeating("Move", timeFactor, repeatFactor);
    }

    public void receiveRotateLeft()
    {
        rotateLeftNeeded = true;
    }

    public void receiveRotateRight()
    {
        rotateRightNeeded = true;
    }
    
    void RotateRight()
    {

        if (dir == Vector3.right)
        {
            dir = Vector3.back;
        }
        else if (dir == Vector3.back)
        {
            dir = Vector3.left;
        }
        else if (dir == Vector3.left)
        {
            dir = Vector3.forward;
        }
        else if (dir == Vector3.forward)
        {
            dir = Vector3.right;
        }

        rotateRightNeeded = false;
    }
    void RotateLeft()
    {

        if (dir == Vector3.right)
        {
            dir = Vector3.forward;
        }
        else if (dir == Vector3.forward)
        {
            dir = Vector3.left;
        }
        else if (dir == Vector3.left)
        {
            dir = Vector3.back;
        }
        else if (dir == Vector3.back)
        {
            dir = Vector3.right;
        }

        rotateLeftNeeded = false;
    }

    void Update()
    {
        if (alive)
        {
            
            if (Input.GetKeyDown(KeyCode.RightArrow)
                || Input.GetKeyDown(KeyCode.E)) {
                RotateRight();
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow)
                || Input.GetKeyDown(KeyCode.Q)) {
                RotateLeft();
            }

            if (rotateLeftNeeded)
            {
                RotateLeft();
                Debug.Log("tried to rotate left");
            }
            else if (rotateRightNeeded)
            {
                RotateRight();
                Debug.Log("tried to rotate right");
            }
        }
        else
        {
            if (Input.GetKey(KeyCode.R))
            {
                foreach (Transform tail in tails)
                {
                    Destroy(tail.gameObject);
                }
                tails.Clear();

                // reset direction and rotation
                sensorsMount.transform.rotation = Quaternion.identity;
                dir = Vector3.right;

                transform.position = new Vector3(0, 0, 0);
                
                alive = true;
            }
        }
    }

    void Move()
    {
        foreach (NewSensorScript sensor in sensors)
        {
            sensor.checkDistance();
        }

        if (alive)
        {
            Vector3 headVector = transform.position;
            
            transform.Translate(dir);
            
            if (hasEaten)
            {
                GameObject g = (GameObject)Instantiate(tailPrefab,
                                  headVector,
                                  Quaternion.identity);
                
                tails.Insert(0, g.transform);
                
                hasEaten = false;
            }
            else if (tails.Count > 0)
            {
                
                tails.Last().position = headVector;
                
                tails.Insert(0, tails.Last());
                tails.RemoveAt(tails.Count - 1);
            }
        }
    }

    void EatFood()
    {
        hasEaten = true;
        foodSpawn.SnakeAteFood();
    }

    void OnTriggerEnter(Collider collided)
    {
        Debug.Log("collision");

        if (collided.gameObject.tag == "Food")
        {

            EatFood();

            Debug.Log("collided with Food");
            
            Destroy(collided.gameObject);
        }
        else if (collided.gameObject.tag != "Plane")
        {
            alive = false;

            Debug.Log("collided with obstacle");
        }
    }
}
