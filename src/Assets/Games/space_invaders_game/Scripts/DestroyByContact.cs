﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {

    public GameObject explosion;
    public GameObject playerExplosion;

    void OnTriggerEnter2D(Collider2D other) {

        if (other.tag == "Boundary") {
            return;
        }

        if (other.tag == "Player") {
            Debug.Log("collision with player");
            if (tag != "PlayerLaser")
            {
                // spawn player's explosion
                Instantiate(playerExplosion, other.transform.position, other.transform.rotation);

                // don't destroy player yet
                // Destroy(other.gameObject);

                //spawn own explosion
                Instantiate(explosion, transform.position, transform.rotation);

                Destroy(gameObject);
            }
            // gameController.GameOver();
            
            return;
        }

        //spawn own explosion
        Instantiate(explosion, transform.position, transform.rotation);
        
        Destroy(gameObject);
    }
}
