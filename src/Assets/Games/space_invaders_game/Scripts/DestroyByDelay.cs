﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByDelay : MonoBehaviour {

    public int destroyTime;

    void Update () {
        Destroy(gameObject, destroyTime);
    }
}
