﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour {

    public GameObject networkController;

	// Use this for initialization
	void Start ()
    {
        networkController = GameObject.FindGameObjectWithTag("NetworkController");
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            networkController.GetComponent<VIPLENetworkController>().closeTCP();
            SceneManager.LoadScene("SnakeGame");
            Application.LoadLevel("SnakeGame");
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            networkController.GetComponent<VIPLENetworkController>().closeTCP();
            SceneManager.LoadScene("SpaceInvaders");
            Application.LoadLevel("SpaceInvaders");
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            networkController.GetComponent<VIPLENetworkController>().closeTCP();
            SceneManager.LoadScene("RunnerGame");
            Application.LoadLevel("RunnerGame");
        }
    }
}
